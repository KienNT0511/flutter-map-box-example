import UIKit
import Flutter
import GoogleMaps

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GMSServices.provideAPIKey("AIzaSyCEeHJWk8OWiQHbW20JJN89VP8R3wJa4sY")
      GeneratedPluginRegistrant.register(with: self)

      weak var registrar = self.registrar(forPlugin: "plugin-name")

      let factory = FLNativeViewFactory(messenger: registrar!.messenger())
      self.registrar(forPlugin: "<plugin-name>")!.register(
          factory,
          withId: "<platform-view-type>")
      
      
      let factory3d = FLNativeView3DFactory(messenger: registrar!.messenger())
      self.registrar(forPlugin: "<plugin-name-3d>")!.register(
        factory3d,
          withId: "<platform-view-type-3D>")
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
