//
//  FLNativeView.swift
//  Runner
//
//  Created by Kiên Nguyễn on 07/11/2022.
//

import Foundation

import Flutter
import UIKit
import SpriteKit

class FLNativeViewFactory: NSObject, FlutterPlatformViewFactory {
    private var messenger: FlutterBinaryMessenger

    init(messenger: FlutterBinaryMessenger) {
        self.messenger = messenger
        super.init()
    }

    func create(
        withFrame frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?
    ) -> FlutterPlatformView {
        return FLNativeView(
            frame: frame,
            viewIdentifier: viewId,
            arguments: args,
            binaryMessenger: messenger)
    }
}

class FLNativeView: NSObject, FlutterPlatformView {
    private var _view: UIView

    init(
        frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?,
        binaryMessenger messenger: FlutterBinaryMessenger?
    ) {
        _view = UIView()
        super.init()
        // iOS views can be created here
        createNativeView(view: _view)
    }

    func view() -> UIView {
        return _view
    }

    func createNativeView(view _view: UIView){
//        _view.backgroundColor = UIColor.blue
//        let nativeLabel = UILabel()
//        nativeLabel.text = "Native text from iOS"
//        nativeLabel.textColor = UIColor.white
//        nativeLabel.textAlignment = .center
//        nativeLabel.frame = CGRect(x: 0, y: 0, width: 180, height: 48.0)
//        _view.addSubview(nativeLabel)
        let skView = SKView(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        let scene = GameScene(size: CGSize(width: 250, height: 250))
        scene.scaleMode = .aspectFill
        scene.backgroundColor = .clear

        skView.presentScene(scene)
        skView.allowsTransparency = true

        skView.ignoresSiblingOrder = true

        //debug info
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.showsDrawCount = true
        _view.addSubview(skView)
    }
}
