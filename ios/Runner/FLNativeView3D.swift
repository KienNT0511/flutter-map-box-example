//
//  FLNativeView.swift
//  Runner
//
//  Created by Kiên Nguyễn on 07/11/2022.
//

import Foundation

import Flutter
import UIKit
import QuartzCore
import SceneKit

class FLNativeView3DFactory: NSObject, FlutterPlatformViewFactory {
    private var messenger: FlutterBinaryMessenger

    init(messenger: FlutterBinaryMessenger) {
        self.messenger = messenger
        super.init()
    }

    func create(
        withFrame frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?
    ) -> FlutterPlatformView {
        return FLNativeView3D(
            frame: frame,
            viewIdentifier: viewId,
            arguments: args,
            binaryMessenger: messenger)
    }
}

class FLNativeView3D: NSObject, FlutterPlatformView, SCNSceneRendererDelegate {
    private var _view: UIView

    var gameView:SCNView!
    var gameScene:SCNScene!
    var cameraNode:SCNNode!
    var targetCreationTime:TimeInterval = 0
    
    init(
        frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?,
        binaryMessenger messenger: FlutterBinaryMessenger?
    ) {
        _view = UIView()
        super.init()
        // iOS views can be created here
        createNativeView(view: _view)
    }

    func view() -> UIView {
        return _view
    }

    func createNativeView(view _view: UIView){
//        let skView = SKView(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
//        let scene = GameScene(size: CGSize(width: 250, height: 250))
//        scene.scaleMode = .aspectFill
//        scene.backgroundColor = .clear
//
//        skView.presentScene(scene)
//        skView.allowsTransparency = true
//
//        skView.ignoresSiblingOrder = true
//
//        //debug info
//        skView.showsFPS = true
//        skView.showsNodeCount = true
//        skView.showsDrawCount = true
//        _view.addSubview(skView)
        initView(view: _view)
        initScene()
        initCamera()
    }
    
    func initView(view _view: UIView){
        gameView = SCNView(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        gameView.allowsCameraControl = true
        gameView.autoenablesDefaultLighting = true
        gameView.backgroundColor = .clear
        
        gameView.delegate = self
        _view.addSubview(gameView)
    }
    
    func initScene (){
        gameScene = SCNScene()
        gameView.scene = gameScene
        
        gameView.isPlaying = true
        
    }
    
    func initCamera() {
        cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        
        cameraNode.position = SCNVector3(x: 0, y:5, z: 10)
        
        gameScene.rootNode.addChildNode(cameraNode)
        
    }
    
    func createTarget() {
    
        let geometry:SCNGeometry = SCNPyramid(width: 1, height: 1, length: 1)
        
        let randomColor = arc4random_uniform(2) == 0 ? UIColor.green : UIColor.red
        
        geometry.materials.first?.diffuse.contents = randomColor
        
        let geometryNode = SCNNode(geometry: geometry)
        geometryNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
        
        if randomColor == UIColor.red {
            geometryNode.name = "enemy"
        }else{
            geometryNode.name = "friend"
        }
        
        gameScene.rootNode.addChildNode(geometryNode)
        
        
        let randomDirection:Float = arc4random_uniform(2) == 0 ? -1.0 : 1.0
        
        let force = SCNVector3(x: randomDirection, y: 15, z: 0)
        
        geometryNode.physicsBody?.applyForce(force, at: SCNVector3(x: 0.05, y: 0.05, z: 0.05), asImpulse: true)
    
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if time > targetCreationTime {
            createTarget()
            targetCreationTime = time + 0.6
        }
        
        cleanUp()
    }
    
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let touch = touches.first!
//        
//        let location = touch.location(in: gameView)
//        
//        let hitList = gameView.hitTest(location, options: nil)
//        
//        if let hitObject = hitList.first {
//            let node = hitObject.node
//            
//            if node.name == "friend" {
//                node.removeFromParentNode()
//                self.gameView.backgroundColor = UIColor.black
//            }else {
//                node.removeFromParentNode()
//                self.gameView.backgroundColor = UIColor.red
//            }
//        }
//        
//    }
    
    
    func cleanUp () {
        for node in gameScene.rootNode.childNodes {
            if node.presentation.position.y < -2 {
                node.removeFromParentNode()
            }
        }
    }
}
