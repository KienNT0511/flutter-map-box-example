//
//  GameScene.swift
//  SpineSampleProject
//
//  Created by Max Gribov on 22/02/2018.
//  Copyright © 2018 Max Gribov. All rights reserved.
//

import SpriteKit
import GameplayKit
import Spine

class GameScene: SKScene {
    
    override func didMove(to view: SKView) {
        
        if let character = Skeleton(fromJSON: "coin-pro", atlas: "coin-pro", skin: "coin-pro"){
            
            character.name = "character"
            character.position = view.center
            
            self.addChild(character)
            
            if let walkAnimation = character.animation(named: "animation") {
                
                character.run(SKAction.repeatForever(walkAnimation))
            }
        }
    }
}
