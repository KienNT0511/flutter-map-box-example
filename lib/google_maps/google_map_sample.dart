import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_example/google_maps/google_map_sample_controller.dart';
import 'package:flutter_example/mapbox/current_user_marker.dart';
import 'package:flutter_example/mapbox/map_box_sample.dart';
import 'package:flutter_example/widgets.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleMapSample extends StatelessWidget {
  GoogleMapSample({super.key});

  final controller = Get.put(GoogleMapSampleController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Obx(
            () => GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: controller.currentPosition.value,
              onMapCreated: controller.onMapCreated,
              onCameraMove: controller.onCameraMove,
              myLocationButtonEnabled: true,
              myLocationEnabled: false,
              padding: EdgeInsets.symmetric(vertical: 40),
              polylines: [
                Polyline(polylineId: PolylineId("1"),
                points: [
                  LatLng(20, 105),
                  LatLng(21, 105),
                  LatLng(22, 105),
                  LatLng(21, 106),
                ]),
              ].toSet(),
            ),
          ),
          _buildCurrentUserMarker()
        ],
      ),
    );
  }

  Widget _buildCurrentUserMarker() {
    return GetBuilder(
      init: controller,
      id: MapBuilderId.userMarker,
      builder: (_) {
        final userMarker = controller.userMakerModel;
        return CurrentUserMarker(
          position: userMarker?.point ?? const Point(0, 0),
        );
      },
    );
  }
}
