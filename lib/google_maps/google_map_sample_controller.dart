import 'dart:math';

import 'package:flutter_example/mapbox/map_box_sample.dart';
import 'package:flutter_example/service/response/user_marker_model.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class GoogleMapSampleController extends GetxController {
  final currentPosition =
      Rx(const CameraPosition(bearing: 300, target: LatLng(0, 0)));
  GoogleMapController? googleMapController;
  final location = Location();

  UserMakerModel? userMakerModel;

  @override
  void onReady() {
    super.onReady();
  }

  Future<void> listenLocationChange() async {
    if (!await requestLocationPermission()) {
      return;
    }

    final position = await location.getLocation();
    currentPosition.value = CameraPosition(
      target: LatLng(position.latitude!, position.longitude!),
      zoom: 15,
    );
    googleMapController
        ?.animateCamera(CameraUpdate.newCameraPosition(currentPosition.value));

    userMakerModel = UserMakerModel(
        latitude: currentPosition.value.target.latitude,
        longitude: currentPosition.value.target.longitude);
    updateUserMarkerPosition();

    location.onLocationChanged.listen((LocationData currentLocation) {
      print(currentLocation.heading);
    });
  }

  Future<bool> requestLocationPermission() async {
    bool serviceEnabled;
    PermissionStatus permissionGranted;

    serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return false;
      }
    }

    permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return false;
      }
    }
    return true;
  }

  void onMapCreated(GoogleMapController mapController) {
    googleMapController = mapController;
    listenLocationChange();
  }

  void onCameraMove(CameraPosition cameraPosition) {
    print(cameraPosition.bearing);
    updateUserMarkerPosition();
  }

  void updateUserMarkerPosition() async {
    try {
      if (userMakerModel == null) {
        return;
      }

      final screenCoordinate = await googleMapController?.getScreenCoordinate(
          LatLng(userMakerModel!.latitude!, userMakerModel!.longitude!));
      userMakerModel?.point = Point(screenCoordinate!.x, screenCoordinate.y);
      update([MapBuilderId.userMarker]);
    } catch (e) {
      print(e);
    }
  }
}
