import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';

class CurrentUserMarker extends StatelessWidget {
  const CurrentUserMarker({super.key, required this.position});

  final Point position;
  final double _iconSize = 24;

  @override
  Widget build(BuildContext context) {
    var ratio = Platform.isIOS ? 1.0 : MediaQuery.of(context).devicePixelRatio;

    return Positioned(
      left: position.x / ratio - _iconSize / 2,
      top: position.y / ratio - _iconSize / 2,
      child: IgnorePointer(
        child: Container(
          width: _iconSize,
          height: _iconSize,
          padding: const EdgeInsets.all(2),
          child: Image.asset(
            "assets/images/test.gif",
            width: 24,
            height: 24,
          ),
        ),
      ),
    );
  }
}
