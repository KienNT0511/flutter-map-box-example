import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_example/mapbox/triangle_painter.dart';
import 'package:flutter_example/service/response/marker_model.dart';

class CustomMarker extends StatefulWidget {
  const CustomMarker({
    super.key,
    this.markerModel,
    required this.position,
  });

  final MarkerModel? markerModel;
  final Point position;

  @override
  State<CustomMarker> createState() => _CustomMarkerState();
}

class _CustomMarkerState extends State<CustomMarker> {
  final double defaultMarkerContainerWidth = 100;

  final double defaultMarkerContainerHeight = 100;

  final double _iconSize = 24;

  @override
  Widget build(BuildContext context) {
    var ratio = 1.0;

    //web does not support Platform._operatingSystem
    if (!kIsWeb) {
      // iOS returns logical pixel while Android returns screen pixel
      ratio = Platform.isIOS ? 1.0 : MediaQuery.of(context).devicePixelRatio;
    }

    return Positioned(
      left: widget.position.x / ratio - defaultMarkerContainerWidth / 2,
      top: widget.position.y / ratio -
          _iconSize / 2 -
          defaultMarkerContainerHeight,
      child: Column(
        children: [
          SizedBox(
            height: defaultMarkerContainerHeight,
            width: defaultMarkerContainerWidth,
            child: Visibility(
              visible: widget.markerModel?.visible == true,
              child: infoWindow(),
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                if (widget.markerModel?.visible == true) {
                  widget.markerModel?.visible = false;
                } else {
                  widget.markerModel?.visible = true;
                }
              });
              print("tap tap tap");
            },
            child: Container(
              width: _iconSize,
              height: _iconSize,
              padding: const EdgeInsets.all(2),
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Colors.yellow[200]),
              child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Colors.yellow[600]),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget infoWindow() {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      alignment: Alignment.topCenter,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.blue,
            ),
            width: 60,
            height: 60,
            child: const Center(child: Text("0.15Km")),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            child: RotatedBox(
              quarterTurns: 2,
              child: CustomPaint(
                painter: TrianglePainter(
                  strokeColor: Colors.blue,
                  strokeWidth: 10,
                  paintingStyle: PaintingStyle.fill,
                ),
                child: const SizedBox(
                  height: 24,
                  width: 10,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
