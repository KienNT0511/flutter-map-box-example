import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_example/mapbox/current_user_marker.dart';
import 'package:flutter_example/mapbox/custom_marker.dart';
import 'package:flutter_example/mapbox/map_box_sample_controller.dart';
import 'package:flutter_example/mapbox/scenic_spots_marker.dart';
import 'package:get/get.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

class MapBoxSample extends StatelessWidget {
  static const String ACCESS_TOKEN = String.fromEnvironment("ACCESS_TOKEN");
  final controller = Get.put(MapBoxSampleController());

  MapBoxSample({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MapBoxExample"),
      ),
      body: Obx(
        () => controller.currentPosition.value != null
            ? Stack(
                children: [
                  _buildMapBox(),
                  _buildScenicSpots(),
                  _buildMarkers(),
                  _buildCurrentUserMarker(),
                  _buildListButtonAction(),
                ],
              )
            : const SizedBox(),
      ),
    );
  }

  void _onMapCreated(MapboxMapController mapboxMapController) {
    controller.setMapboxController(mapboxMapController);
  }

  void _onMapLongClickCallback(Point<double> point, LatLng coordinates) {
    controller.addMarker(
        point: point,
        latitude: coordinates.latitude,
        longitude: coordinates.longitude);
  }

  void _onCameraIdleCallback() {
    controller.updateAllMarker();
  }

  void _onStyleLoadedCallback() {
    print('onStyleLoadedCallback');
  }

  Widget _buildMapBox() {
    return MapboxMap(
      accessToken: ACCESS_TOKEN,
      trackCameraPosition: true,
      onMapCreated: _onMapCreated,
      onMapLongClick: _onMapLongClickCallback,
      onCameraIdle: _onCameraIdleCallback,
      onStyleLoadedCallback: _onStyleLoadedCallback,
      myLocationEnabled: false,
      minMaxZoomPreference: const MinMaxZoomPreference(0, 16),
      initialCameraPosition: controller.currentPosition.value!,
    );
  }

  Widget _buildScenicSpots() {
    return GetBuilder(
      init: controller,
      id: MapBuilderId.scenicMakers,
      builder: (_) {
        return IgnorePointer(
          child: Stack(
            children: List.generate(controller.listScenic.length, (index) {
              final scenicModel = controller.listScenic[index];
              return ScenicSpotsMarker(
                key: GlobalKey(),
                scenicModel: scenicModel,
                position: scenicModel.point ?? const Point(0, 0),
                cameraZoom: controller.mapController?.cameraPosition?.zoom,
              );
            }),
          ),
        );
      },
    );
  }

  Widget _buildListButtonAction() {
    return Row(
      children: [
        Obx(() => controller.isRecord.value
            ? _buildButtonStop()
            : _buildButtonStart()),
        const SizedBox(
          width: 10,
        ),
        _buildButtonClear(),
      ],
    );
  }

  Widget _buildButtonStart() {
    return InkWell(
      onTap: () {
        controller.startRecord();
      },
      child: Container(
        alignment: Alignment.center,
        width: 70,
        height: 50,
        color: Colors.blue[300],
        child: const Text("Start"),
      ),
    );
  }

  Widget _buildButtonStop() {
    return InkWell(
      onTap: () {
        controller.stopRecord();
      },
      child: Container(
        alignment: Alignment.center,
        width: 70,
        height: 50,
        color: Colors.red,
        child: const Text("Stop"),
      ),
    );
  }

  Widget _buildButtonClear() {
    return InkWell(
      onTap: () {
        controller.clear();
      },
      child: Container(
        alignment: Alignment.center,
        width: 70,
        height: 50,
        color: Colors.pinkAccent,
        child: const Text("Clear"),
      ),
    );
  }

  Widget _buildCurrentUserMarker() {
    return GetBuilder(
      init: controller,
      id: MapBuilderId.userMarker,
      builder: (_) {
        final userMarker = controller.userMakerModel;
        return CurrentUserMarker(
          position: userMarker?.point ?? const Point(0, 0),
        );
      },
    );
  }

  Widget _buildMarkers() {
    return GetBuilder(
      init: controller,
      id: MapBuilderId.markers,
      builder: (_) {
        return Stack(
          children: List.generate(controller.listMarker.length, (index) {
            final marker = controller.listMarker[index];
            return CustomMarker(
              markerModel: marker,
              position: marker.point ?? const Point(0, 0),
            );
          }),
        );
      },
    );
  }
}

class MapBuilderId {
  static const String markers = "markers";
  static const String scenicMakers = "scenicMakers";
  static const String userMarker = "userMaker";
}
