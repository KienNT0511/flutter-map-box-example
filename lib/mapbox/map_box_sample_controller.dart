import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/services.dart';
import 'package:flutter_example/mapbox/map_box_sample.dart';
import 'package:flutter_example/service/response/marker_model.dart';
import 'package:flutter_example/service/response/scenic_model.dart';
import 'package:flutter_example/service/response/user_marker_model.dart';
import 'package:flutter_example/service/storage_service.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

class MapBoxSampleController extends GetxController {
  MapboxMapController? mapController;
  List<MarkerModel> listMarker = [];

  List<ScenicModel> listScenic = [];

  UserMakerModel? userMakerModel;

  Rx<CameraPosition?> currentPosition =
      Rx(StorageService().getLastCameraPosition());
  var geometry = <LatLng>[];
  Line? line;
  RxBool isRecord = false.obs;
  Location location = Location();

  void setMapboxController(MapboxMapController mapboxMapController) {
    mapController = mapboxMapController;
    mapController?.addListener(() {
      if (mapController?.isCameraMoving == true) {
        updateAllMarker();
      }
    });
  }

  void updateAllMarker() {
    updateUserMarkerPosition();
    updateMarkerPosition();
    updateScenicMakerPosition();
  }

  @override
  void onReady() async {
    super.onReady();
    listenLocationChange();
  }

  Future<void> listenLocationChange() async {
    if (!await requestLocationPermission()) {
      return;
    }

    addMarker(
        point: const Point(0, 0),
        latitude: 20.992273769271137,
        longitude: 105.78498319864491);
    getScenicSpots();

    final position = await location.getLocation();
    currentPosition.value = CameraPosition(
      target: LatLng(position.latitude!, position.longitude!),
      zoom: 15,
    );
    StorageService().saveLastCameraPosition(currentPosition.value);
    userMakerModel = UserMakerModel(
        latitude: currentPosition.value?.target.latitude,
        longitude: currentPosition.value?.target.longitude);
    mapController
        ?.animateCamera(CameraUpdate.newCameraPosition(currentPosition.value!));
    updateUserMarkerPosition();

    location.onLocationChanged.listen((LocationData currentLocation) {
      try {
        currentPosition.value = CameraPosition(
          target: LatLng(currentLocation.latitude!, currentLocation.longitude!),
          zoom: currentPosition.value?.zoom ?? 15,
        );
        StorageService().saveLastCameraPosition(currentPosition.value);
        print(
            "lat: ${currentLocation.latitude} =========== long: ${currentLocation.longitude}");

        if (!isRecord.value) {
          return;
        }

        if (geometry.isEmpty) {
          geometry.add(currentPosition.value!.target);
        }

        userMakerModel = UserMakerModel(
            latitude: currentLocation.latitude,
            longitude: currentLocation.longitude);
        updateUserMarkerPosition();

        print("distance: ${distanceCalculator(
          source: geometry.last,
          destination: LatLng(
            currentLocation.latitude!,
            currentLocation.longitude!,
          ),
        )}");

        geometry
            .add(LatLng(currentLocation.latitude!, currentLocation.longitude!));

        updateLine();
      } catch (e) {
        print(e);
      }
    });
  }

  Future<void> addLine() async {
    line = await mapController?.addLine(LineOptions(
        geometry: geometry,
        lineColor: "#ff0000",
        lineWidth: 5.0,
        lineOpacity: 0.5,
        lineJoin: "round",
        draggable: false));
  }

  Future<void> updateLine() async {
    if (line == null) {
      return;
    }

    final lineOptions = LineOptions(
        geometry: geometry,
        lineColor: "#ff0000",
        lineWidth: 5.0,
        lineOpacity: 0.5,
        lineJoin: "round",
        draggable: false);
    // mapController?.updateLine(line!, lineOptions);
    mapController?.clearLines();
    line = await mapController?.addLine(lineOptions);
    print("update line");
  }

  void startRecord() {
    isRecord.value = true;
    location.changeSettings(
        accuracy: LocationAccuracy.high, interval: 1000, distanceFilter: 0);
    location.enableBackgroundMode(enable: true);
    addLine();
  }

  void stopRecord() async {
    isRecord.value = false;
    location.changeSettings(
        accuracy: LocationAccuracy.high, interval: 1000, distanceFilter: 0);
    location.enableBackgroundMode(enable: false);
  }

  void clear() async {
    isRecord.value = false;
    location.changeSettings(
        accuracy: LocationAccuracy.high, interval: 1000, distanceFilter: 0);
    location.enableBackgroundMode(enable: false);
    geometry.clear();
    mapController?.clearLines();
  }

  void updateUserMarkerPosition() {
    try {
      if (userMakerModel == null) {
        return;
      }

      final coordinates = <LatLng>[
        LatLng(userMakerModel!.latitude!, userMakerModel!.longitude!)
      ];

      mapController?.toScreenLocationBatch(coordinates).then((points) {
        userMakerModel?.point = points.first;
      });
      update([MapBuilderId.userMarker]);
    } catch (e) {
      print(e);
    }
  }

  void updateMarkerPosition() {
    try {
      if (listMarker.isEmpty) {
        return;
      }

      final coordinates = <LatLng>[];

      for (final maker in listMarker) {
        coordinates.add(LatLng(maker.latitude!, maker.longitude!));
      }

      mapController?.toScreenLocationBatch(coordinates).then((points) {
        listMarker.asMap().forEach((i, value) {
          listMarker[i].point = points[i];
        });
      });
      update([MapBuilderId.markers]);
    } catch (e) {
      print(e);
    }
  }

  void addMarker({
    Point<double>? point,
    double? latitude,
    double? longitude,
  }) {
    final marker = MarkerModel(latitude: latitude, longitude: longitude);
    marker.point = point;
    listMarker.add(marker);
    updateMarkerPosition();
  }

  Future<void> getScenicSpots() async {
    print("getScenicSpots");
    final data =
        await rootBundle.loadString("assets/scenic_spots.json", cache: false);
    final mapData = jsonDecode(data);
    if (mapData is List) {
      listScenic = mapData.map((e) => ScenicModel.fromJson(e)).toList();
    }
    updateScenicMakerPosition();
  }

  void updateScenicMakerPosition() {
    try {
      if (listScenic.isEmpty) {
        return;
      }

      final coordinates = <LatLng>[];

      for (final scenic in listScenic) {
        coordinates.add(LatLng(scenic.latitude!, scenic.longitude!));
      }

      mapController?.toScreenLocationBatch(coordinates).then((points) {
        listScenic.asMap().forEach((i, value) {
          listScenic[i].point = points[i];
        });
      });
      update([MapBuilderId.scenicMakers]);
    } catch (e) {
      print(e);
    }
  }

  Future<bool> requestLocationPermission() async {
    bool serviceEnabled;
    PermissionStatus permissionGranted;

    serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return false;
      }
    }

    permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return false;
      }
    }
    return true;
  }

  /// use to test distance between two coordinates
  /// return meters
  double distanceCalculator({LatLng? source, LatLng? destination}) {
    try {
      const earthRadius = 6378137.0;
      final latDelta = degreeToRad(destination!.latitude - source!.latitude);
      final longDelta = degreeToRad(destination.longitude - source.longitude);
      final latRad = degreeToRad(source.latitude);
      final longRad = degreeToRad(destination.latitude);
      final angel = pow(sin(latDelta / 2), 2) +
          pow(sin(longDelta / 2), 2) * cos(latRad) * cos(longRad);
      final constant = 2 * asin(sqrt(angel));
      return earthRadius * constant;
    } catch (e) {
      return 0.0;
    }
  }

  double degreeToRad(double degree) {
    return degree * pi / 180;
  }
}
