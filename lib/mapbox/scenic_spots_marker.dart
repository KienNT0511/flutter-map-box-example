import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_example/service/response/scenic_model.dart';

class ScenicSpotsMarker extends StatelessWidget {
  const ScenicSpotsMarker({
    super.key,
    this.scenicModel,
    required this.position,
    this.cameraZoom,
  });

  final ScenicModel? scenicModel;

  final double defaultMarkerContainerWidth = 100;

  final double defaultMarkerContainerHeight = 100;

  final Point position;

  final double? cameraZoom;

  @override
  Widget build(BuildContext context) {
    var ratio = 1.0;

    //web does not support Platform._operatingSystem
    if (!kIsWeb) {
      // iOS returns logical pixel while Android returns screen pixel
      ratio = Platform.isIOS ? 1.0 : MediaQuery.of(context).devicePixelRatio;
    }

    return Positioned(
            left: position.x / ratio - defaultMarkerContainerWidth / 2,
            top: position.y / ratio,
            child: showMarker() ? Container(
              constraints:
                  BoxConstraints(maxWidth: defaultMarkerContainerWidth),
              color: Colors.red[200],
              padding: const EdgeInsets.all(2),
              child: Text(
                "${scenicModel?.name}",
                textAlign: TextAlign.center,
              ),
            ) : const SizedBox(),
          );
  }

  bool showMarker() {
    try {
      return cameraZoom! >= scenicModel!.maxZoom!;
    } catch (e) {
      return false;
    }
  }
}
