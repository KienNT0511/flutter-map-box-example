import 'package:flutter_example/service/api/api_path.dart';
import 'package:flutter_example/service/api/api_service.dart';
import 'package:flutter_example/service/response/directions.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

class MapBoxRepository {
  static Future<Directions> getDirections({
    DirectionsProfile profile = DirectionsProfile.driving,
    required LatLng currentUserPosition,
    required LatLng targetPosition,
  }) async {
    final response = await ApiService().get(
      ApiPath.getDirections(
          profile: profile,
          currentUserPosition: currentUserPosition,
          targetPosition: targetPosition),
    );

    return Directions.fromJson(response.data);
  }
}
