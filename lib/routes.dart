import 'package:flutter/material.dart';
import 'package:flutter_example/google_maps/google_map_sample.dart';
import 'package:flutter_example/mapbox/map_box_sample.dart';
import 'package:flutter_example/native_view.dart';
import 'package:get/get.dart';

import 'native_view_3d.dart';

class RouteName {
  static const String root = "/";
  static const String googleMaps = "/google-maps";
  static const String mapbox = "/mapbox";
  static const String nativeView = "/native_view";
  static const String nativeView3D = "/native_view_3d";
}

class AppRoutes {
  static Widget getScreen(RouteSettings settings) {
    switch (settings.name) {
      case RouteName.root:
        return Scaffold(
          body: SafeArea(
            child: Column(
              children: [
                TextButton(
                    onPressed: () {
                      Get.toNamed(RouteName.googleMaps);
                    },
                    child: Text("Google maps")),
                SizedBox(
                  height: 10,
                ),
                TextButton(
                    onPressed: () {
                      Get.toNamed(RouteName.mapbox);
                    },
                    child: Text("Mapbox")),
                TextButton(
                    onPressed: () {
                      Get.toNamed(RouteName.nativeView);
                    },
                    child: Text("native view")),
                TextButton(
                    onPressed: () {
                      Get.toNamed(RouteName.nativeView3D);
                    },
                    child: Text("native view 3d")),
              ],
            ),
          ),
        );
      case RouteName.googleMaps:
        return GoogleMapSample();
      case RouteName.mapbox:
        return MapBoxSample();
      case RouteName.nativeView:
        return const IOSCompositionWidget();
      case RouteName.nativeView3D:
        return const IOS3DCompositionWidget();
      default:
        return getDefaultScreen();
    }
  }

  static Widget getDefaultScreen() => const Scaffold(
        body: Center(child: Text('Undefined')),
      );

  static GetPageRoute generateRoute(RouteSettings settings) {
    return GetPageRoute(
      settings: settings,
      page: () => getScreen(settings),
    );
  }
}
