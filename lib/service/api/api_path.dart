import 'package:mapbox_gl/mapbox_gl.dart';

class ApiPath {
  static String getDirections(
      {required DirectionsProfile profile,
      required LatLng currentUserPosition,
      required LatLng targetPosition}) {
    final coordinates =
        "${currentUserPosition.getLongLatString()};${targetPosition.getLongLatString()}";
    return "/directions/v5/${profile.getProfileString()}/$coordinates";
  }
}

extension LatLngExtensions on LatLng {
  String getLongLatString() {
    return "$longitude,$latitude";
  }
}

enum DirectionsProfile { drivingTraffic, driving, walking, cycling }

extension DirectionsProfileExtensions on DirectionsProfile {
  String getProfileString() {
    switch (this) {
      case DirectionsProfile.drivingTraffic:
        return "mapbox/driving-traffic";
      case DirectionsProfile.walking:
        return "mapbox/walking";
      case DirectionsProfile.cycling:
        return "mapbox/cycling";
      default:
        return "mapbox/driving";
    }
  }
}
