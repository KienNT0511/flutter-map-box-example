import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_example/service/api/app_interceptor.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class ApiService extends DioMixin {
  static final ApiService _service = ApiService._internal();

  ApiService._internal() {
    options = BaseOptions(
      baseUrl: "https://api.mapbox.com",
      connectTimeout: 30000,
    );
    options.headers.putIfAbsent("Content-Type", () => "application/json");
    options.headers.putIfAbsent("Accept", () => "application/json");
    interceptors.add(AppInterceptor());
    if (kDebugMode) {
      interceptors.add(PrettyDioLogger());
    }
    httpClientAdapter = DefaultHttpClientAdapter();
    (httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
  }

  factory ApiService() => _service;
}
