import 'package:dio/dio.dart';

class AppInterceptor extends Interceptor {
  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    const accessToken = String.fromEnvironment("ACCESS_TOKEN");
    options.queryParameters["access_token"] = accessToken;
    super.onRequest(options, handler);
  }
}
