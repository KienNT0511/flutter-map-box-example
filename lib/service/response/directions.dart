class Directions {
  List<Routes>? routes;
  List<Waypoints>? waypoints;
  String? code;
  String? uuid;

  Directions({this.routes, this.waypoints, this.code, this.uuid});

  Directions.fromJson(Map<String, dynamic> json) {
    if (json['routes'] != null) {
      routes = <Routes>[];
      json['routes'].forEach((v) {
        routes!.add(Routes.fromJson(v));
      });
    }
    if (json['waypoints'] != null) {
      waypoints = <Waypoints>[];
      json['waypoints'].forEach((v) {
        waypoints!.add(Waypoints.fromJson(v));
      });
    }
    code = json['code'];
    uuid = json['uuid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (routes != null) {
      data['routes'] = routes!.map((v) => v.toJson()).toList();
    }
    if (waypoints != null) {
      data['waypoints'] = waypoints!.map((v) => v.toJson()).toList();
    }
    data['code'] = code;
    data['uuid'] = uuid;
    return data;
  }
}

class Routes {
  String? geometry;
  List<Legs>? legs;
  String? weightName;
  double? weight;
  double? duration;
  double? distance;

  Routes(
      {this.geometry,
      this.legs,
      this.weightName,
      this.weight,
      this.duration,
      this.distance});

  Routes.fromJson(Map<String, dynamic> json) {
    geometry = json['geometry'];
    if (json['legs'] != null) {
      legs = <Legs>[];
      json['legs'].forEach((v) {
        legs!.add(Legs.fromJson(v));
      });
    }
    weightName = json['weight_name'];
    weight = json['weight'];
    duration = json['duration'];
    distance = json['distance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['geometry'] = geometry;
    if (legs != null) {
      data['legs'] = legs!.map((v) => v.toJson()).toList();
    }
    data['weight_name'] = weightName;
    data['weight'] = weight;
    data['duration'] = duration;
    data['distance'] = distance;
    return data;
  }
}

class Legs {
  String? summary;
  double? weight;
  double? duration;
  List<Steps>? steps;
  double? distance;

  Legs({this.summary, this.weight, this.duration, this.steps, this.distance});

  Legs.fromJson(Map<String, dynamic> json) {
    summary = json['summary'];
    weight = json['weight'];
    duration = json['duration'];
    if (json['steps'] != null) {
      steps = <Steps>[];
      json['steps'].forEach((v) {
        steps!.add(Steps.fromJson(v));
      });
    }
    distance = json['distance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['summary'] = summary;
    data['weight'] = weight;
    data['duration'] = duration;
    if (steps != null) {
      data['steps'] = steps!.map((v) => v.toJson()).toList();
    }
    data['distance'] = distance;
    return data;
  }
}

class Steps {
  List<Intersections>? intersections;
  String? drivingSide;
  String? geometry;
  String? mode;
  Maneuver? maneuver;
  String? ref;
  double? weight;
  double? duration;
  String? name;
  double? distance;
  List<VoiceInstructions>? voiceInstructions;
  List<BannerInstructions>? bannerInstructions;

  Steps(
      {this.intersections,
      this.drivingSide,
      this.geometry,
      this.mode,
      this.maneuver,
      this.ref,
      this.weight,
      this.duration,
      this.name,
      this.distance,
      this.voiceInstructions,
      this.bannerInstructions});

  Steps.fromJson(Map<String, dynamic> json) {
    if (json['intersections'] != null) {
      intersections = <Intersections>[];
      json['intersections'].forEach((v) {
        intersections!.add(Intersections.fromJson(v));
      });
    }
    drivingSide = json['driving_side'];
    geometry = json['geometry'];
    mode = json['mode'];
    maneuver =
        json['maneuver'] != null ? Maneuver.fromJson(json['maneuver']) : null;
    ref = json['ref'];
    weight = json['weight'];
    duration = json['duration'];
    name = json['name'];
    distance = json['distance'];
    if (json['voiceInstructions'] != null) {
      voiceInstructions = <VoiceInstructions>[];
      json['voiceInstructions'].forEach((v) {
        voiceInstructions!.add(VoiceInstructions.fromJson(v));
      });
    }
    if (json['bannerInstructions'] != null) {
      bannerInstructions = <BannerInstructions>[];
      json['bannerInstructions'].forEach((v) {
        bannerInstructions!.add(BannerInstructions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (intersections != null) {
      data['intersections'] = intersections!.map((v) => v.toJson()).toList();
    }
    data['driving_side'] = drivingSide;
    data['geometry'] = geometry;
    data['mode'] = mode;
    if (maneuver != null) {
      data['maneuver'] = maneuver!.toJson();
    }
    data['ref'] = ref;
    data['weight'] = weight;
    data['duration'] = duration;
    data['name'] = name;
    data['distance'] = distance;
    if (voiceInstructions != null) {
      data['voiceInstructions'] =
          voiceInstructions!.map((v) => v.toJson()).toList();
    }
    if (bannerInstructions != null) {
      data['bannerInstructions'] =
          bannerInstructions!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Intersections {
  int? intersectionsOut;
  List<bool>? entry;
  List<int>? bearings;
  List<double>? location;
  int? intersectionsIn;

  Intersections(
      {this.intersectionsOut,
      this.entry,
      this.bearings,
      this.location,
      this.intersectionsIn});

  Intersections.fromJson(Map<String, dynamic> json) {
    intersectionsOut = json['out'];
    entry = json['entry'].cast<bool>();
    bearings = json['bearings'].cast<int>();
    location = json['location'].cast<double>();
    intersectionsIn = json['in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['out'] = intersectionsOut;
    data['entry'] = entry;
    data['bearings'] = bearings;
    data['location'] = location;
    data['in'] = intersectionsIn;
    return data;
  }
}

class Maneuver {
  int? bearingAfter;
  int? bearingBefore;
  List<double>? location;
  String? modifier;
  String? type;
  String? instruction;

  Maneuver(
      {this.bearingAfter,
      this.bearingBefore,
      this.location,
      this.modifier,
      this.type,
      this.instruction});

  Maneuver.fromJson(Map<String, dynamic> json) {
    bearingAfter = json['bearing_after'];
    bearingBefore = json['bearing_before'];
    location = json['location'].cast<double>();
    modifier = json['modifier'];
    type = json['type'];
    instruction = json['instruction'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bearing_after'] = bearingAfter;
    data['bearing_before'] = bearingBefore;
    data['location'] = location;
    data['modifier'] = modifier;
    data['type'] = type;
    data['instruction'] = instruction;
    return data;
  }
}

class VoiceInstructions {
  double? distanceAlongGeometry;
  String? announcement;
  String? ssmlAnnouncement;

  VoiceInstructions(
      {this.distanceAlongGeometry, this.announcement, this.ssmlAnnouncement});

  VoiceInstructions.fromJson(Map<String, dynamic> json) {
    distanceAlongGeometry = json['distanceAlongGeometry'];
    announcement = json['announcement'];
    ssmlAnnouncement = json['ssmlAnnouncement'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['distanceAlongGeometry'] = distanceAlongGeometry;
    data['announcement'] = announcement;
    data['ssmlAnnouncement'] = ssmlAnnouncement;
    return data;
  }
}

class BannerInstructions {
  double? distanceAlongGeometry;
  Primary? primary;
  Primary? secondary;
  dynamic sub;

  BannerInstructions(
      {this.distanceAlongGeometry, this.primary, this.secondary, this.sub});

  BannerInstructions.fromJson(Map<String, dynamic> json) {
    distanceAlongGeometry = json['distanceAlongGeometry'];
    primary =
        json['primary'] != null ? Primary.fromJson(json['primary']) : null;
    secondary =
        json['secondary'] != null ? Primary.fromJson(json['secondary']) : null;
    sub = json['sub'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['distanceAlongGeometry'] = distanceAlongGeometry;
    if (primary != null) {
      data['primary'] = primary!.toJson();
    }
    if (secondary != null) {
      data['secondary'] = secondary!.toJson();
    }
    data['sub'] = sub;
    return data;
  }
}

class Primary {
  String? text;
  List<Components>? components;
  String? type;
  String? modifier;

  Primary({this.text, this.components, this.type, this.modifier});

  Primary.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    if (json['components'] != null) {
      components = <Components>[];
      json['components'].forEach((v) {
        components!.add(Components.fromJson(v));
      });
    }
    type = json['type'];
    modifier = json['modifier'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['text'] = text;
    if (components != null) {
      data['components'] = components!.map((v) => v.toJson()).toList();
    }
    data['type'] = type;
    data['modifier'] = modifier;
    return data;
  }
}

class Components {
  String? text;

  Components({this.text});

  Components.fromJson(Map<String, dynamic> json) {
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['text'] = text;
    return data;
  }
}

class Waypoints {
  String? name;
  List<double>? location;

  Waypoints({this.name, this.location});

  Waypoints.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    location = json['location'].cast<double>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['location'] = location;
    return data;
  }
}
