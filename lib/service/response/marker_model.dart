import 'dart:math';

class MarkerModel {
  double? latitude;
  double? longitude;
  Point? point;
  bool? visible;

  MarkerModel({this.latitude, this.longitude});

  MarkerModel.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    return data;
  }
}
