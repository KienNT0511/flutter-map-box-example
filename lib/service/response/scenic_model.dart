import 'dart:math';

class ScenicModel {
  double? latitude;
  double? longitude;
  String? name;
  double? maxZoom;
  Point? point;

  ScenicModel({this.latitude, this.longitude, this.name});

  ScenicModel.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longitude = json['longitude'];
    name = json['name'];
    maxZoom = json['max-zoom'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['max-zoom'] = maxZoom;
    return data;
  }
}
