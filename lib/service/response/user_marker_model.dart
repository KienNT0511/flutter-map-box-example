import 'dart:math';

class UserMakerModel {
  double? latitude;
  double? longitude;
  Point? point;

  UserMakerModel({this.latitude, this.longitude});

  UserMakerModel.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    return data;
  }
}
