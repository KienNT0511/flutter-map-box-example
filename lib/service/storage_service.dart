import 'dart:convert';

import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum StorageServiceKey {
  lastCameraPosition,
}

class StorageService {
  SharedPreferences? _prefs;
  static final StorageService _service = StorageService._internal();

  StorageService._internal();

  factory StorageService() => _service;

  Future<void> init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  Future<void> saveLastCameraPosition(CameraPosition? cameraPosition) async {
    final dataMap = cameraPosition?.toMap();
    final jsonString = jsonEncode(dataMap);
    _prefs?.setString(StorageServiceKey.lastCameraPosition.name, jsonString);
  }

  CameraPosition getLastCameraPosition() {
    try {
      final jsonString =
          _prefs?.getString(StorageServiceKey.lastCameraPosition.name);
      final dataMap = jsonDecode(jsonString!);
      final cameraPosition = CameraPosition(
        bearing: dataMap['bearing'],
        target: LatLng(dataMap['target'][0], dataMap['target'][1]),
        tilt: dataMap['tilt'],
        zoom: dataMap['zoom'],
      );
      return cameraPosition;
    } catch (e) {
      return const CameraPosition(target: LatLng(0, 0));
    }
  }
}
