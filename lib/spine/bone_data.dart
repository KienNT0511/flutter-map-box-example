/// ****************************************************************************
/// Spine Runtimes License Agreement
/// Last updated September 24, 2021. Replaces all prior versions.
///
/// Copyright (c) 2013-2021, Esoteric Software LLC
///
/// Integration of the Spine Runtimes into software or otherwise creating
/// derivative works of the Spine Runtimes is permitted under the terms and
/// conditions of Section 2 of the Spine Editor License Agreement:
/// http://esotericsoftware.com/spine-editor-license
///
/// Otherwise, it is permitted to integrate the Spine Runtimes into software
/// or otherwise create derivative works of the Spine Runtimes (collectively,
/// "Products"), provided that each user of the Products must obtain their own
/// Spine Editor license and redistribution of the Products in any form must
/// include this license and copyright notice.
///
/// THE SPINE RUNTIMES ARE PROVIDED BY ESOTERIC SOFTWARE LLC "AS IS" AND ANY
/// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
/// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
/// DISCLAIMED. IN NO EVENT SHALL ESOTERIC SOFTWARE LLC BE LIABLE FOR ANY
/// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
/// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES,
/// BUSINESS INTERRUPTION, OR LOSS OF USE, DATA, OR PROFITS) HOWEVER CAUSED AND
/// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
/// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
/// THE SPINE RUNTIMES, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
///****************************************************************************/
import 'package:flutter/material.dart';

/// Stores the setup pose for a {@link Bone}. */
class BoneData {
  final int index;
  final String name;
  BoneData? parent;
  double? length;
  double? x, y, rotation, scaleX = 1, scaleY = 1, shearX, shearY;
  TransformMode transformMode = TransformMode.normal;
  bool? skinRequired;

  // Nonessential.
  final Color color = const Color.fromRGBO(155, 155, 255, 1); // 9b9b9bff

  BoneData(this.index, this.name, this.parent) {
    if (index < 0 && name.isNotEmpty) {
      throw ArgumentError('index must be >= 0.');
    }
  }

  BoneData copyWith(BoneData bone, BoneData? parent) {
    BoneData newBone = BoneData(bone.index, bone.name, parent);
    newBone.length = bone.length;
    newBone.x = bone.x;
    newBone.y = bone.y;
    newBone.rotation = bone.rotation;
    newBone.scaleX = bone.scaleX;
    newBone.scaleY = bone.scaleY;
    newBone.shearX = bone.shearX;
    newBone.shearY = bone.shearY;
    return newBone;
  }

  BoneData? getParent() {
    return parent;
  }

  void setPosition(double x, double y) {
    this.x = x;
    this.y = y;
  }

  void setScale(double scaleX, double scaleY) {
    this.scaleX = scaleX;
    this.scaleY = scaleY;
  }
}

/// Determines how a bone inherits world transforms from parent bones. */
enum TransformMode {
  normal,
  onlyTranslation,
  noRotationOrReflection,
  noScale,
  noScaleOrReflection
}
